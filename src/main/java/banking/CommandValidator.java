package banking;

public class CommandValidator {
    protected Bank bank;


    protected CommandValidator(Bank bank) {
        this.bank = bank;
    }

    public boolean calculateValidator(String command) {
        CreateCommandValidator createCommandValidator = new CreateCommandValidator(bank);
        DepositCommandValidator depositCommandValidator = new DepositCommandValidator(bank);
        WithdrawCommandValidator withdrawCommandValidator = new WithdrawCommandValidator(bank);
        PassTimeCommandValidator passTimeCommandValidator = new PassTimeCommandValidator(bank);
        TransferCommandValidator transferCommandValidator = new TransferCommandValidator(bank, depositCommandValidator, withdrawCommandValidator);
        String[] splitCommand = command.split(" ");
        String commandType = splitCommand[0].toLowerCase();
        switch (commandType) {
            case "create":
                return createCommandValidator.validate(command);
            case "deposit":
                return depositCommandValidator.validate(command);
            case "withdraw":
                return withdrawCommandValidator.validate(command);
            case "pass":
                return passTimeCommandValidator.validate(command);
            case "transfer":
                return transferCommandValidator.validate(command);
            default:
                return false;
        }
    }

    public boolean isIDRightLength(String command) {
        return command.length() == 8;
    }

    public boolean idExists(String command) {
        return bank.accountExistsByID(command);
    }

    public boolean isInteger(String input) {
        try {
            Integer.parseInt(input);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean isFloat(String input) {
        try {
            Float.parseFloat(input);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean isDouble(String input) {
        try {
            Double.parseDouble(input);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
