package banking;

public class DepositCommandProcessor extends CommandProcessor {
    public DepositCommandProcessor(Bank bank) {
        super(bank);
    }

    public void processInput(String[] splitInput) {
        splitInput = removeCommandFromInput(splitInput);
        String depositAmount = splitInput[1];
        String depositId = splitInput[0];
        bank.deposit(depositId, depositAmount);
    }

}
