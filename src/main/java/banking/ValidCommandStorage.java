package banking;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class ValidCommandStorage {
    public Map<String, ArrayList<String>> storage;
    Bank bank;

    ValidCommandStorage(Bank bank) {
        this.bank = bank;
        this.storage = new LinkedHashMap<>();
    }


    public void updateStorage(String command) {
        removeNonExistentAccounts();
        String[] splitCommand = command.split(" ");
        String function = splitCommand[0];
        if (function.toLowerCase().equals("create")) {
            String id = splitCommand[2];
            storage.computeIfAbsent(bank.getAccounts().get(id).getId(), k -> new ArrayList<>());
        } else if (function.toLowerCase().equals("deposit") || function.toLowerCase().equals("withdraw")) {
            String id = splitCommand[1];
            storage.get(bank.getAccounts().get(id).getId()).add(command);
        } else if (function.toLowerCase().equals("transfer")) {
            String idOne = splitCommand[1];
            String idTwo = splitCommand[2];
            storage.get(bank.getAccounts().get(idOne).getId()).add(command);
            storage.get(bank.getAccounts().get(idTwo).getId()).add(command);
        }

    }

    public Map<String, ArrayList<String>> getStorage() {
        return storage;
    }

    public void removeNonExistentAccounts() {
        storage.entrySet().removeIf(account -> bank.getAccounts().get(account.getKey()) == null);
    }

    public ArrayList<String> outputStorage() {
        ArrayList<String> output = new ArrayList<>();
        for (Map.Entry<String, ArrayList<String>> account : storage.entrySet()) {
            output.add(bank.getAccounts().get(account.getKey()).getStatus());
            output.addAll(account.getValue());
        }
        return output;
    }
}
