package banking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TransferCommandValidator extends CommandValidator {
    DepositCommandValidator depositCommandValidator;
    WithdrawCommandValidator withdrawCommandValidator;

    public TransferCommandValidator(Bank bank, DepositCommandValidator depositCommandValidator, WithdrawCommandValidator withdrawCommandValidator) {
        super(bank);
        this.depositCommandValidator = depositCommandValidator;
        this.withdrawCommandValidator = withdrawCommandValidator;
    }

    public boolean validate(String command) {
        String[] splitCommand = command.split(" ");
        List<String> depositCommand = new ArrayList<>(Arrays.asList(splitCommand));
        List<String> withdrawCommand = new ArrayList<>(Arrays.asList(splitCommand));
        if (boundsParser(splitCommand)) {
            depositCommand.remove(1);
            withdrawCommand.remove(2);
            String depositCommandString;
            String withdrawCommandString;
            depositCommandString = setupCommandString(depositCommand).toString();
            withdrawCommandString = setupCommandString(withdrawCommand).toString();
            return withdrawCommandValidator.validate(withdrawCommandString) && depositCommandValidator.validate(depositCommandString);
        }
        return false;
    }

    private boolean boundsParser(String[] splitCommand) {
        return splitCommand.length == 4;
    }

    private StringBuilder setupCommandString(List<String> command) {
        StringBuilder commandString = new StringBuilder();
        for (String s : command) {
            commandString.append(s).append(" ");
        }
        return commandString;
    }
}
