package banking;

public class CreateCommandProcessor extends CommandProcessor {

    public CreateCommandProcessor(Bank bank) {
        super(bank);
    }

    public void processInput(String[] splitInput) {
        splitInput = removeCommandFromInput(splitInput);
        String id = splitInput[1];
        String type = splitInput[0];
        float apr = Float.parseFloat(splitInput[2]);
        switch (type.toLowerCase()) {
            case "checking":
                Account checking = new Checking(id, apr);
                bank.addAccount(id, checking);
                break;
            case "savings":
                Account savings = new Savings(id, apr);
                bank.addAccount(id, savings);
                break;
            default:
                double deposit = Integer.parseInt(splitInput[3]);
                Account cd = new CD(id, apr, deposit);
                bank.addAccount(id, cd);
                break;
        }
    }
}
