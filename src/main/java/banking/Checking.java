package banking;

public class Checking extends Account {

    Checking(String id, float apr) {
        super(id, apr);
        type = "Checking";
    }
}
