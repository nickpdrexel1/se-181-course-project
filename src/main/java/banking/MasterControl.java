package banking;

import java.util.ArrayList;
import java.util.List;

public class MasterControl {
    Bank bank;
    CommandValidator commandValidator;
    CommandProcessor commandProcessor;
    CommandStorage commandStorage;
    ValidCommandStorage validCommandStorage;

    public MasterControl(Bank bank, CommandValidator commandValidator, CommandProcessor commandProcessor, CommandStorage commandStorage, ValidCommandStorage validCommandStorage) {
        this.bank = bank;
        this.commandValidator = commandValidator;
        this.commandProcessor = commandProcessor;
        this.commandStorage = commandStorage;
        this.validCommandStorage = validCommandStorage;

    }

    public List<String> start(List<String> command) {
        for (String input : command) {
            if (commandValidator.calculateValidator(input)) {
                commandProcessor.calculateProcessor(input);
                validCommandStorage.updateStorage(input);

            } else {
                commandStorage.addToInvalidStorage(input);
            }
        }
        ArrayList<String> finalOutput = new ArrayList<>();
        finalOutput.addAll(validCommandStorage.outputStorage());
        finalOutput.addAll(commandStorage.getInvalidCommands());
        return finalOutput;
    }
}
