package banking;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TransferCommandProcessor extends CommandProcessor {
    DepositCommandProcessor depositCommandProcessor;
    WithdrawCommandProcessor withdrawCommandProcessor;

    public TransferCommandProcessor(Bank bank, WithdrawCommandProcessor withdrawCommandProcessor, DepositCommandProcessor depositCommandProcessor) {
        super(bank);
        this.withdrawCommandProcessor = withdrawCommandProcessor;
        this.depositCommandProcessor = depositCommandProcessor;
    }

    public void processInput(String[] splitInput) {
        BigDecimal withdrawBalance = bank.getAccounts().get(splitInput[1]).getBalance();
        BigDecimal withdrawAmount = new BigDecimal(splitInput[3]);
        String actualWithdraw = setActualWithdraw(withdrawAmount, withdrawBalance);
        List<String> depositCommand = new ArrayList<>(Arrays.asList(splitInput));
        List<String> withdrawCommand = new ArrayList<>(Arrays.asList(splitInput));
        withdrawCommandProcessor.processInput(setupWithdrawStringArray(withdrawCommand, actualWithdraw));
        depositCommandProcessor.processInput(setupDepositStringArray(depositCommand, actualWithdraw));
    }

    public String setActualWithdraw(BigDecimal withdrawAmount, BigDecimal withdrawBalance) {
        if (withdrawAmount.compareTo(withdrawBalance) > 0) {
            return withdrawBalance.toString();
        } else {
            return withdrawAmount.toString();
        }
    }

    public String[] setupDepositStringArray(List<String> depositCommand, String actualWithdraw) {
        depositCommand.remove(1);
        depositCommand.remove(2);
        depositCommand.add(actualWithdraw);
        return addToStringArray(depositCommand);
    }

    public String[] setupWithdrawStringArray(List<String> withdrawCommand, String actualWithdraw) {
        withdrawCommand.remove(2);
        withdrawCommand.remove(2);
        withdrawCommand.add(actualWithdraw);
        return addToStringArray(withdrawCommand);
    }

    public String[] addToStringArray(List<String> command) {
        String[] stringArray = new String[command.size()];
        for (int pos = 0; pos < command.size(); pos++) {
            stringArray[pos] = command.get(pos);
        }
        return stringArray;
    }
}
