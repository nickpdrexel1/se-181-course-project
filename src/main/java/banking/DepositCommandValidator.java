package banking;

public class DepositCommandValidator extends CommandValidator {

    public DepositCommandValidator(Bank bank) {
        super(bank);
    }

    public boolean validate(String command) {
        String[] splitCommand = command.split(" ");
        if (boundsParser(splitCommand)) {
            if (bank.getAccounts().get(splitCommand[1]).type.equals("Savings")) {
                return savingsBoundsChecker(splitCommand);
            } else if (bank.getAccounts().get(splitCommand[1]).type.equals("Checking")) {
                return checkingBoundsChecker(splitCommand);
            }
        }
        return false;
    }

    public boolean boundsParser(String[] splitCommand) {
        if (checkCommandLength(splitCommand)) {
            return idBounds(splitCommand) && isDouble(splitCommand[2]);
        }

        return false;
    }

    public boolean idBounds(String[] splitCommand) {
        return idExists(splitCommand[1]);
    }

    public boolean savingsBoundsChecker(String[] splitCommand) {
        return Double.parseDouble(splitCommand[2]) <= 2500 && Double.parseDouble(splitCommand[2]) >= 0;
    }

    public boolean checkingBoundsChecker(String[] splitCommand) {
        return Double.parseDouble(splitCommand[2]) <= 1000 && Double.parseDouble(splitCommand[2]) >= 0;
    }

    public boolean checkCommandLength(String[] splitCommand) {
        return splitCommand.length == 3;
    }
}
