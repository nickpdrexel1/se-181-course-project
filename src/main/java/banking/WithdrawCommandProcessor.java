package banking;

public class WithdrawCommandProcessor extends CommandProcessor {
    public WithdrawCommandProcessor(Bank bank) {
        super(bank);
    }

    public void processInput(String[] splitInput) {
        String withdrawId = splitInput[1];
        String withdrawAmount = splitInput[2];
        bank.withdraw(withdrawId, withdrawAmount);
    }
}
