package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class CommandProcessorTest {
    public static final String INPUT1 = "create checking 12345678 1.0";
    public static final String INPUT2 = "create savings 12345679 1.0";
    public static final String INPUT3 = "create cd 12345688 1.0 1000";
    public static final String DINPUT1 = "deposit 12345678 678";
    public static final String DINPUT2 = "deposit 12345679 678";
    public static final String DINPUT3 = "deposit 12345678 99";
    public static final String DINPUT4 = "deposit 12345679 99";
    public static final String TINPUT1 = "pass 1";
    public static final String TINPUT5 = "pass 5";
    public static final String WINPUT1 = "withdraw 12345678 200";
    public static final String WINPUT2 = "withdraw 12345679 200";

    Bank bank;
    CommandProcessor commandProcessor;
    int timePassed;

    @BeforeEach
    void setUp() {
        bank = new Bank();
        timePassed = PassTime.getCurrentTime();
        commandProcessor = new CommandProcessor(bank);
        commandProcessor.calculateProcessor(INPUT1);
        commandProcessor.calculateProcessor(INPUT2);
        commandProcessor.calculateProcessor(INPUT3);
    }

    @Test
    void pass_time_one_month() {
        commandProcessor.calculateProcessor(TINPUT1);
        assertEquals(timePassed + 1, PassTime.getCurrentTime());
    }

    @Test
    void pass_time_one_month_check_apr_multiple_accounts() {
        commandProcessor.calculateProcessor(DINPUT1);
        commandProcessor.calculateProcessor(DINPUT2);
        commandProcessor.calculateProcessor(TINPUT1);
        assertEquals(timePassed + 1, PassTime.getCurrentTime());
        assertEquals(new BigDecimal("678.56"), bank.getAccounts().get("12345678").getBalance());
        assertEquals(new BigDecimal("678.56"), bank.getAccounts().get("12345679").getBalance());
        assertEquals(new BigDecimal("1003.33"), bank.getAccounts().get("12345688").getBalance());
    }

    @Test
    void pass_time_one_month_check_apr_checking() {
        commandProcessor.calculateProcessor(DINPUT1);
        commandProcessor.calculateProcessor(TINPUT1);
        assertEquals(timePassed + 1, PassTime.getCurrentTime());
        assertEquals(new BigDecimal("678.56"), bank.getAccounts().get("12345678").getBalance());
    }

    @Test
    void pass_time_one_month_check_apr_savings() {
        commandProcessor.calculateProcessor(DINPUT2);
        commandProcessor.calculateProcessor(TINPUT1);
        assertEquals(timePassed + 1, PassTime.getCurrentTime());
        assertEquals(new BigDecimal("678.56"), bank.getAccounts().get("12345679").getBalance());
    }

    @Test
    void pass_time_one_month_check_apr_cd() {
        commandProcessor.calculateProcessor(TINPUT1);
        assertEquals(timePassed + 1, PassTime.getCurrentTime());
        assertEquals(new BigDecimal("1003.33"), bank.getAccounts().get("12345688").getBalance());
    }

    @Test
    void checking_account_actually_loses_balance_if_below_hundred_when_time_passed() {
        commandProcessor.calculateProcessor(DINPUT3);
        commandProcessor.calculateProcessor(TINPUT1);
        assertEquals(new BigDecimal("74.06"), bank.getAccounts().get("12345678").getBalance());
    }

    @Test
    void savings_account_actually_loses_balance_if_below_hundred_when_time_passed() {
        commandProcessor.calculateProcessor(DINPUT4);
        commandProcessor.calculateProcessor(TINPUT1);
        assertEquals(new BigDecimal("74.06"), bank.getAccounts().get("12345679").getBalance());
    }

    @Test
    void checking_account_balance_zero_or_below_removed_when_time_passed() {
        commandProcessor.calculateProcessor("create checking 12345789 1.0");
        commandProcessor.calculateProcessor("deposit 12345789 25");
        commandProcessor.calculateProcessor(TINPUT1);
        assertNull(bank.getAccounts().get("12345789"));
    }

    @Test
    void savings_account_balance_zero_or_below_removed_when_time_passed() {
        commandProcessor.calculateProcessor("create savings 12345798 1.0");
        commandProcessor.calculateProcessor("create savings 12345799 1.0");
        commandProcessor.calculateProcessor("deposit 12345798 25");
        commandProcessor.calculateProcessor("deposit 12345799 101");
        commandProcessor.calculateProcessor(TINPUT1);
        assertNull(bank.getAccounts().get("12345798"));
    }

    @Test
    void pass_time_five_months_check_apr_cd() {
        commandProcessor.calculateProcessor(TINPUT5);
        assertEquals(timePassed + 5, PassTime.getCurrentTime());
        assertEquals(new BigDecimal("1016.79"), bank.getAccounts().get("12345688").getBalance());
    }

    @Test
    void check_withdraw_checking() {
        commandProcessor.calculateProcessor(DINPUT1);
        commandProcessor.calculateProcessor(WINPUT1);
        assertEquals(new BigDecimal("478.00"), bank.getAccounts().get("12345678").getBalance());
    }

    @Test
    void check_withdraw_savings() {
        commandProcessor.calculateProcessor(DINPUT2);
        commandProcessor.calculateProcessor(WINPUT2);
        assertEquals(new BigDecimal("478.00"), bank.getAccounts().get("12345679").getBalance());
    }

    @Test
    void check_withdraw_from_savings_sets_correct_month() {
        commandProcessor.calculateProcessor(DINPUT2);
        commandProcessor.calculateProcessor(WINPUT2);
        Savings checkSavings = (Savings) bank.getAccounts().get("12345679");
        assertEquals(new BigDecimal("478.00"), bank.getAccounts().get("12345679").getBalance());
        assertEquals(banking.PassTime.getCurrentTime(), checkSavings.getLastWithdrawMonth());
    }

    @Test
    void withdraw_one_month_then_another() {
        commandProcessor.calculateProcessor(DINPUT2);
        commandProcessor.calculateProcessor(WINPUT2);
        assertEquals(new BigDecimal("478.00"), bank.getAccounts().get("12345679").getBalance());
        commandProcessor.calculateProcessor(TINPUT1);
        commandProcessor.calculateProcessor(WINPUT2);
        assertEquals(new BigDecimal("278.39"), bank.getAccounts().get("12345679").getBalance());
    }

    @Test
    void Withdraw_greater_than_account_size() {
        commandProcessor.calculateProcessor(DINPUT4);
        commandProcessor.calculateProcessor(WINPUT2);
        assertEquals(new BigDecimal("0.00"), bank.getAccounts().get("12345679").getBalance());
    }

    @Test
    void transfer_money_from_a_to_b() {
        commandProcessor.calculateProcessor(DINPUT1);
        commandProcessor.calculateProcessor("transfer 12345678 12345679 100");
        assertEquals(new BigDecimal("578.00"), bank.getAccounts().get("12345678").getBalance());
        assertEquals(new BigDecimal("100.00"), bank.getAccounts().get("12345679").getBalance());

    }

    @Test
    void transfer_no_money_from_a_to_b() {
        commandProcessor.calculateProcessor(DINPUT1);
        commandProcessor.calculateProcessor("transfer 12345678 12345679 0");
        assertEquals(new BigDecimal("678.00"), bank.getAccounts().get("12345678").getBalance());
        assertEquals(new BigDecimal("0.00"), bank.getAccounts().get("12345679").getBalance());

    }

    @Test
    void transfer_money_from_ghost_account_to_actual() {
        commandProcessor.calculateProcessor(DINPUT1);
        commandProcessor.calculateProcessor("transfer 12345679 12345678 100");
        assertEquals(new BigDecimal("678.00"), bank.getAccounts().get("12345678").getBalance());
        assertEquals(new BigDecimal("0.00"), bank.getAccounts().get("12345679").getBalance());

    }

    @Test
    void transfer_money_from_ghost_to_ghost() {
        commandProcessor.calculateProcessor("transfer 12345679 12345678 100");
        assertEquals(new BigDecimal("0.00"), bank.getAccounts().get("12345678").getBalance());
        assertEquals(new BigDecimal("0.00"), bank.getAccounts().get("12345679").getBalance());
    }

    @Test
    void transfer_money_larger_than_in_account() {
        commandProcessor.calculateProcessor("deposit 12345679 50");
        commandProcessor.calculateProcessor("transfer 12345679 12345678 100");
        assertEquals(new BigDecimal("50.00"), bank.getAccounts().get("12345678").getBalance());
        assertEquals(new BigDecimal("0.00"), bank.getAccounts().get("12345679").getBalance());
    }

    @Test
    void transfer_money_pass_time() {
        commandProcessor.calculateProcessor(DINPUT1);
        commandProcessor.calculateProcessor("transfer 12345678 12345679 100");
        commandProcessor.calculateProcessor(TINPUT1);
        assertEquals(new BigDecimal("578.48"), bank.getAccounts().get("12345678").getBalance());
        assertEquals(new BigDecimal("100.08"), bank.getAccounts().get("12345679").getBalance());
    }

    @Test
    void withdraw_from_savings_pass_time_transfer_money() {
        commandProcessor.calculateProcessor(DINPUT2);
        commandProcessor.calculateProcessor(WINPUT2);
        commandProcessor.calculateProcessor(DINPUT1);
        commandProcessor.calculateProcessor(TINPUT1);
        commandProcessor.calculateProcessor("transfer 12345679 12345678 100");
        assertEquals(new BigDecimal("778.56"), bank.getAccounts().get("12345678").getBalance());
        assertEquals(new BigDecimal("378.39"), bank.getAccounts().get("12345679").getBalance());
    }

}