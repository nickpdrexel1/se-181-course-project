package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ValidCommandStorageTest {
    ValidCommandStorage validCommandStorage;
    Account account;
    Account account1;
    String idOne;
    String idTwo;
    ArrayList<String> expected;

    @BeforeEach
    void setUp() {
        account = new Checking("10340313", .56f);
        account1 = new Savings("10340314", .56f);
        Bank bank = new Bank();
        bank.addAccount("10340313", account);
        bank.addAccount("10340314", account1);
        idOne = bank.getAccounts().get("10340313").getId();
        idTwo = bank.getAccounts().get("10340314").getId();
        validCommandStorage = new ValidCommandStorage(bank);
    }

    @Test
    void add_an_account_to_storage() {
        validCommandStorage.updateStorage("create checking 10340313 .56");
        assertEquals(validCommandStorage.getStorage().get(idOne), new ArrayList<String>());
    }

    @Test
    void add_two_accounts_to_storage() {
        validCommandStorage.updateStorage("create checking 10340313 .56");
        validCommandStorage.updateStorage("create savings 10340314 .56");
        assertEquals(validCommandStorage.getStorage().get(idOne), new ArrayList<String>());
        assertEquals(validCommandStorage.getStorage().get(idTwo), new ArrayList<String>());
    }

    @Test
    void add_a_deposit_command_to_an_account() {
        validCommandStorage.updateStorage("create checking 10340313 .56");
        validCommandStorage.updateStorage("deposit 10340313 100");
        assertEquals(validCommandStorage.getStorage().get(idOne).get(0), "deposit 10340313 100");
    }

    @Test
    void add_a_withdraw_command_to_an_account() {
        validCommandStorage.updateStorage("create checking 10340313 .56");
        validCommandStorage.updateStorage("withdraw 10340313 100");
        assertEquals(validCommandStorage.getStorage().get(idOne).get(0), "withdraw 10340313 100");
    }

    @Test
    void add_a_transfer_command_to_an_account() {
        validCommandStorage.updateStorage("create checking 10340313 .56");
        validCommandStorage.updateStorage("create savings 10340314 .56");
        validCommandStorage.updateStorage("transfer 10340313 10340314 100");
        assertEquals(validCommandStorage.getStorage().get(idOne).get(0), "transfer 10340313 10340314 100");
        assertEquals(validCommandStorage.getStorage().get(idTwo).get(0), "transfer 10340313 10340314 100");
    }

    @Test
    void output_status_one_account() {
        expected = new ArrayList<>();
        validCommandStorage.updateStorage("create checking 10340313 .56");
        expected.add("Checking 10340313 0.00 0.56");
        assertEquals(expected, validCommandStorage.outputStorage());
    }

    @Test
    void add_and_update_account_check_output() {
        expected = new ArrayList<>();
        validCommandStorage.updateStorage("create checking 10340313 .56");
        validCommandStorage.updateStorage("deposit 10340313 100");
        expected.add("Checking 10340313 0.00 0.56");
        expected.add("deposit 10340313 100");
        assertEquals(expected, validCommandStorage.outputStorage());
    }

    @Test
    void add_two_accounts_update_check_output() {
        expected = new ArrayList<>();
        validCommandStorage.updateStorage("create checking 10340313 .56");
        validCommandStorage.updateStorage("deposit 10340313 100");
        validCommandStorage.updateStorage("create savings 10340314 .56");
        validCommandStorage.updateStorage("deposit 10340314 100");
        expected.add("Checking 10340313 0.00 0.56");
        expected.add("deposit 10340313 100");
        expected.add("Savings 10340314 0.00 0.56");
        expected.add("deposit 10340314 100");
        assertEquals(expected, validCommandStorage.outputStorage());
    }
}
