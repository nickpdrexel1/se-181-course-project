package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CommandValidatorTest {
    CommandValidator commandValidator;
    Bank bank;

    @BeforeEach
    void setUp() {
        bank = new Bank();
        commandValidator = new CommandValidator(bank);
    }

    @Test
    void invalid_command() {
        boolean actual = commandValidator.calculateValidator("wack savings 12345678 0.6");
        assertFalse(actual);
    }

    @Test
    void valid_create_command() {
        boolean actual = commandValidator.calculateValidator("Create savings 12345678 0.6");
        assertTrue(actual);
    }

    @Test
    void valid_deposit_command() {
        Account savings = new Savings("12345678", 0.6f);
        bank.addAccount(savings.getId(), savings);
        boolean actual = commandValidator.calculateValidator("deposit 12345678 100");
        assertTrue(actual);
    }

    @Test
    void valid_pass_time_command() {
        boolean actual = commandValidator.calculateValidator("pass 1");
        assertTrue(actual);
    }

    @Test
    void valid_withdraw_commands() {
        Account savings = new Savings("12345678", 0.6f);
        Account checking = new Checking("12345679", 0.6f);
        Account cd = new CD("12345688", 0.6f, 1000);
        bank.addAccount(savings.getId(), savings);
        bank.addAccount(checking.getId(), checking);
        bank.addAccount(cd.getId(), cd);
        boolean actual = commandValidator.calculateValidator("withdraw 12345678 1");
        boolean actual1 = commandValidator.calculateValidator("withdraw 12345679 1");
        boolean actual2 = commandValidator.calculateValidator("withdraw 12345688 1");
        assertTrue(actual);
        assertTrue(actual1);
        assertFalse(actual2);

    }

    @Test
    void correct_transfer_command() {
        Account savings = new Savings("12345678", 0.6f);
        Account checking = new Checking("12345679", 0.6f);
        bank.addAccount(savings.getId(), savings);
        bank.addAccount(checking.getId(), checking);
        boolean actual = commandValidator.calculateValidator("transfer 12345678 12345679 100");
        assertTrue(actual);
    }

    @Test
    void check_all_commands() {
        boolean actual = commandValidator.calculateValidator("Create savings 12345678 0.6");
        Account savings = new Savings("12345678", 0.6f);
        Account checking = new Checking("12345679", 0.6f);
        bank.addAccount(savings.getId(), savings);
        bank.addAccount(checking.getId(), checking);
        boolean actual1 = commandValidator.calculateValidator("deposit 12345678 100");
        boolean actual2 = commandValidator.calculateValidator("withdraw 12345678 1");
        boolean actual3 = commandValidator.calculateValidator("pass 1");
        boolean actual4 = commandValidator.calculateValidator("transfer 12345678 12345679 100");
        assertTrue(actual);
        assertTrue(actual1);
        assertTrue(actual2);
        assertTrue(actual3);
        assertTrue(actual4);
    }
}

