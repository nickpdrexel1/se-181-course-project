package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreateCommandProcessorTest {
    public static final String[] INPUT1 = new String[]{"create", "checking", "12345678", "1.0"};
    public static final String[] INPUT2 = new String[]{"create", "savings", "12345679", "1.0"};
    public static final String[] INPUT3 = new String[]{"create", "cd", "12345688", "1.0", "1000"};

    Bank bank;
    CreateCommandProcessor createCommandProcessor;

    @BeforeEach
    void setUp() {
        bank = new Bank();
        createCommandProcessor = new CreateCommandProcessor(bank);
        createCommandProcessor.processInput(INPUT1);
        createCommandProcessor.processInput(INPUT2);
        createCommandProcessor.processInput(INPUT3);
    }

    @Test
    void check_create_checking() {
        assertEquals("Checking", bank.getAccounts().get("12345678").getAccountType());
        assertEquals(1.0f, bank.getAccounts().get("12345678").getApr());
        assertEquals("12345678", bank.getAccounts().get("12345678").getId());
    }

    @Test
    void check_create_savings() {
        createCommandProcessor.processInput(INPUT2);
        assertEquals("Savings", bank.getAccounts().get("12345679").getAccountType());
        assertEquals(1.0f, bank.getAccounts().get("12345679").getApr());
        assertEquals("12345679", bank.getAccounts().get("12345679").getId());
    }

    @Test
    void check_create_cd() {
        createCommandProcessor.processInput(INPUT3);
        assertEquals("Cd", bank.getAccounts().get("12345688").getAccountType());
        assertEquals(1.0f, bank.getAccounts().get("12345688").getApr());
        assertEquals("12345688", bank.getAccounts().get("12345688").getId());
        assertEquals(new BigDecimal("1000.00"), bank.getAccounts().get("12345688").getBalance());
    }
}
