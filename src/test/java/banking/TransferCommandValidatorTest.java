package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TransferCommandValidatorTest {
    DepositCommandValidator depositCommandValidator;
    WithdrawCommandValidator withdrawCommandValidator;
    TransferCommandValidator transferCommandValidator;
    Bank bank;

    @BeforeEach
    void setUp() {
        bank = new Bank();
        withdrawCommandValidator = new WithdrawCommandValidator(bank);
        depositCommandValidator = new DepositCommandValidator(bank);
        transferCommandValidator = new TransferCommandValidator(bank, depositCommandValidator, withdrawCommandValidator);
    }

    @Test
    void check_length_transfer_command() {
        Account savings = new Savings("12345678", 0.6f);
        Account checking = new Checking("12345677", 0.6f);
        bank.addAccount(savings.getId(), savings);
        bank.addAccount(checking.getId(), checking);
        boolean actual = transferCommandValidator.validate("transfer");
        boolean actual1 = transferCommandValidator.calculateValidator("transfer 12345678");
        boolean actual2 = transferCommandValidator.calculateValidator("transfer 12345678 12345677");
        boolean actual3 = transferCommandValidator.calculateValidator("transfer 12345678 12345677 100 10334");
        assertFalse(actual);
        assertFalse(actual1);
        assertFalse(actual2);
        assertFalse(actual3);
    }

    @Test
    void transfer_money_from_a_to_b() {
        Account savings = new Savings("12345678", 0.6f);
        Account checking = new Checking("12345677", 0.6f);
        bank.addAccount(savings.getId(), savings);
        bank.addAccount(checking.getId(), checking);
        boolean actual = transferCommandValidator.calculateValidator("transfer 12345678 12345677 100");
        assertTrue(actual);
    }

    @Test
    void try_bounds_for_transfer_from_cd_to_any_account() {
        Account cd = new CD("12345678", 0.6f, 1001);
        Account checking = new Checking("12345677", 0.6f);
        Account savings = new Savings("12345679", 0.6f);
        bank.addAccount(cd.getId(), cd);
        bank.addAccount(checking.getId(), checking);
        bank.addAccount(savings.getId(), savings);
        bank.deposit("12345677", "101");
        bank.deposit("12345679", "101");
        boolean actual = transferCommandValidator.calculateValidator("transfer 12345678 12345677 100");
        boolean actual1 = transferCommandValidator.calculateValidator("transfer 12345678 12345677 1000");
        boolean actual2 = transferCommandValidator.calculateValidator("transfer 12345678 12345677 1001");
        PassTime.passTime(12);
        boolean actual5 = transferCommandValidator.calculateValidator("transfer 12345678 12345677 100");
        boolean actual3 = transferCommandValidator.calculateValidator("transfer 12345678 12345677 1100");
        boolean actual4 = transferCommandValidator.calculateValidator("transfer 12345678 12345679 1100");
        assertFalse(actual);
        assertFalse(actual1);
        assertFalse(actual2);
        assertFalse(actual5);
        assertFalse(actual3);
        assertTrue(actual4);
    }

    @Test
    void withdraw_from_savings_transfer_money() {
        Account checking = new Checking("12345678", 0.6f);
        Savings savings = new Savings("12345679", 0.6f);
        bank.addAccount(checking.getId(), checking);
        bank.addAccount(savings.getId(), savings);
        bank.deposit("12345679", "101");
        bank.withdraw("12345679", "100");
        boolean actual = transferCommandValidator.calculateValidator("transfer 12345679 12345678 100");
        assertFalse(actual);
        assertEquals(PassTime.getCurrentTime(), savings.getLastWithdrawMonth());
    }

    @Test
    void try_transfer_from_none_to_an_account() {
        Account savings = new Savings("12345679", 0.6f);
        bank.addAccount(savings.getId(), savings);
        bank.deposit("12345679", "101");
        boolean actual = transferCommandValidator.calculateValidator("transfer 12345678 12345679 100");
        assertFalse(actual);
    }

    @Test
    void try_transfer_from_an_account_to_none() {
        Account checking = new Checking("12345677", 0.6f);
        bank.addAccount(checking.getId(), checking);
        bank.deposit("12345677", "101");
        boolean actual = transferCommandValidator.calculateValidator("transfer 12345677 12345679 100");
        assertFalse(actual);
    }

    @Test
    void withdraw_from_savings_pass_time_transfer_money() {
        Savings savings = new Savings("12345679", 0.6f);
        bank.addAccount(savings.getId(), savings);
        bank.deposit("12345679", "201");
        bank.withdraw("12345679", "100");
        PassTime.passTime(1);
        Account checking = new Checking("12345678", 0.6f);
        bank.addAccount(checking.getId(), checking);
        boolean actual = transferCommandValidator.calculateValidator("transfer 12345679 12345678 100");
        assertTrue(actual);
    }
}
