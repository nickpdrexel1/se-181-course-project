package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateCommandValidatorTest {

    CreateCommandValidator createCommandValidator;
    Bank bank;

    @BeforeEach
    void setUp() {
        bank = new Bank();
        createCommandValidator = new CreateCommandValidator(bank);
    }

    @Test
    void weird_case_case_create_command() {
        boolean actual = createCommandValidator.validate("CrEaTe savings 12345678 0.6");
        assertTrue(actual);
    }

    @Test
    void weird_case_type_create_command() {
        boolean actual = createCommandValidator.validate("Create saViNgs 12345678 0.6");
        assertTrue(actual);
    }

    @Test
    void invalid_length_create_command() {
        boolean actual = createCommandValidator.validate("Create savings 12345678 0.6 ooga");
        boolean actual1 = createCommandValidator.validate("Create savings 12345678");
        boolean actual2 = createCommandValidator.validate("Create savings");
        boolean actual3 = createCommandValidator.validate("Create");
        boolean actual4 = createCommandValidator.validate("Create cd 12345678 0.6 100 abc");
        boolean actual5 = createCommandValidator.validate("Create cd 12345678 0.6");
        assertFalse(actual);
        assertFalse(actual1);
        assertFalse(actual2);
        assertFalse(actual3);
        assertFalse(actual4);
        assertFalse(actual5);

    }

    @Test
    void invalid_duplicate_id_create_command() {
        Account savings = new Savings("12345678", 0.6f);
        bank.addAccount(savings.getId(), savings);
        boolean actual = createCommandValidator.validate("Create savings 12345678 0.6");
        assertFalse(actual);
    }

    @Test
    void invalid_id_length_create_command() {
        boolean actual = createCommandValidator.validate("Create savings 1234567 0.6");
        boolean actual1 = createCommandValidator.validate("Create savings 123456789 0.6");
        assertFalse(actual);
        assertFalse(actual1);
    }

    @Test
    void invalid_id_type_create_command() {
        boolean actual = createCommandValidator.validate("Create savings 1a3b5c7d 0.6");
        boolean actual1 = createCommandValidator.validate("Create savings 12345678 0.6");
        assertFalse(actual);
        assertTrue(actual1);
    }

    @Test
    void invalid_size_apr() {
        boolean actual = createCommandValidator.validate("Create savings 12345678 11");
        boolean actual1 = createCommandValidator.validate("Create savings 12345678 10");
        assertFalse(actual);
        assertTrue(actual1);
    }

    @Test
    void create_account_type() {
        boolean actual = createCommandValidator.validate("Create savings 12345678 0.6");
        boolean actual1 = createCommandValidator.validate("Create checking 22345678 0.6");
        boolean actual2 = createCommandValidator.validate("Create cd 32345678 0.6 2000");
        assertTrue(actual);
        assertTrue(actual1);
        assertTrue(actual2);
    }

    @Test
    void check_creation_invalid_account_type() {
        boolean actual = createCommandValidator.validate("Create hevver 42345678 0.6");
        assertFalse(actual);
    }

    @Test
    void check_creation_value() {
        boolean actual = createCommandValidator.validate("Create savings 12345678 0.6");
        boolean actual1 = createCommandValidator.validate("Create checking 22345678 0.6");
        assertTrue(actual);
        assertTrue(actual1);
    }

    @Test
    void check_account_checking_savings_deposit() {
        boolean actual = createCommandValidator.validate("Create savings 12345678 0.6 342");
        boolean actual1 = createCommandValidator.validate("Create checking 22345678 0.6 6234");
        assertFalse(actual);
        assertFalse(actual1);
    }

    @Test
    void check_account_with_deposit() {
        boolean actual = createCommandValidator.validate("Create cd 12345678 0.6");
        boolean actual1 = createCommandValidator.validate("Create cd 12345678 0.6 999");
        boolean actual2 = createCommandValidator.validate("Create cd 22345678 0.6 1000");
        boolean actual3 = createCommandValidator.validate("Create cd 22345678 0.6 1001");
        boolean actual4 = createCommandValidator.validate("Create cd 22345678 0.6 9999");
        boolean actual5 = createCommandValidator.validate("Create cd 22345678 0.6 10000");
        boolean actual6 = createCommandValidator.validate("Create cd 22345678 0.6 10001");
        assertFalse(actual);
        assertFalse(actual1);
        assertTrue(actual2);
        assertTrue(actual3);
        assertTrue(actual4);
        assertTrue(actual5);
        assertFalse(actual6);
    }

    @Test
    void check_apr_is_float() {
        boolean actual = createCommandValidator.validate("Create checking 12345678 0.6");
        boolean actual1 = createCommandValidator.validate("Create checking 22345678 2fdf");
        assertTrue(actual);
        assertFalse(actual1);
    }
}
