package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class WithdrawCommandValidatorTest {
    WithdrawCommandValidator withdrawCommandValidator;
    Bank bank;

    @BeforeEach
    void setUp() {
        bank = new Bank();
        withdrawCommandValidator = new WithdrawCommandValidator(bank);
    }

    @Test
    void valid_withdraw_commands() {
        Account savings = new Savings("12345678", 0.6f);
        Account checking = new Checking("12345679", 0.6f);
        Account cd = new CD("12345688", 0.6f, 1000);
        bank.addAccount(savings.getId(), savings);
        bank.addAccount(checking.getId(), checking);
        bank.addAccount(cd.getId(), cd);
        boolean actual = withdrawCommandValidator.validate("withdraw 12345678 1");
        boolean actual1 = withdrawCommandValidator.validate("withdraw 12345679 1");
        boolean actual2 = withdrawCommandValidator.validate("withdraw 12345688 1");
        assertTrue(actual);
        assertTrue(actual1);
        assertFalse(actual2);

    }

    @Test
    void invalid_withdraw_command_invalid_withdraw() {
        Account savings = new Savings("12345678", 0.6f);
        bank.addAccount(savings.getId(), savings);
        boolean actual = withdrawCommandValidator.validate("withdraw 12345678 ooga");
        assertFalse(actual);
    }

    @Test
    void invalid_withdraw_command_invalid_lengths() {
        Account savings = new Savings("12345678", 0.6f);
        bank.addAccount(savings.getId(), savings);
        boolean actual = withdrawCommandValidator.validate("withdraw 12345678 1 ooga");
        boolean actual1 = withdrawCommandValidator.validate("withdraw 12345678");
        assertFalse(actual);
        assertFalse(actual1);
    }

    @Test
    void bounding_amounts_for_savings() {
        Account savings = new Savings("12345678", 0.6f);
        bank.addAccount(savings.getId(), savings);
        boolean actual = withdrawCommandValidator.validate("withdraw 12345678 -1");
        boolean actual1 = withdrawCommandValidator.validate("withdraw 12345678 0");
        boolean actual2 = withdrawCommandValidator.validate("withdraw 12345678 1");
        boolean actual3 = withdrawCommandValidator.validate("withdraw 12345678 999");
        boolean actual4 = withdrawCommandValidator.validate("withdraw 12345678 1000");
        boolean actual5 = withdrawCommandValidator.validate("withdraw 12345678 1001");
        assertFalse(actual);
        assertFalse(actual1);
        assertTrue(actual2);
        assertTrue(actual3);
        assertTrue(actual4);
        assertFalse(actual5);


    }

    @Test
    void bounding_amounts_for_checking() {
        Account checking = new Checking("12345678", 0.6f);
        bank.addAccount(checking.getId(), checking);
        boolean actual = withdrawCommandValidator.validate("withdraw 12345678 -1");
        boolean actual1 = withdrawCommandValidator.validate("withdraw 12345678 0");
        boolean actual2 = withdrawCommandValidator.validate("withdraw 12345678 1");
        boolean actual3 = withdrawCommandValidator.validate("withdraw 12345678 399");
        boolean actual4 = withdrawCommandValidator.validate("withdraw 12345678 400");
        boolean actual5 = withdrawCommandValidator.validate("withdraw 12345678 401");
        assertFalse(actual);
        assertFalse(actual1);
        assertTrue(actual2);
        assertTrue(actual3);
        assertTrue(actual4);
        assertFalse(actual5);
    }

    @Test
    void two_withdraws_savings_fails() {
        Savings savings = new Savings("12345678", 0.6f);
        bank.addAccount(savings.getId(), savings);
        boolean actual = withdrawCommandValidator.validate("withdraw 12345678 100");
        savings.setLastWithdrawMonth();
        boolean actual1 = withdrawCommandValidator.validate("withdraw 12345678 1");
        assertTrue(actual);
        assertFalse(actual1);
    }

    @Test
    void withdraw_from_cd_before_and_after_twelve_months() {
        CD cd = new CD("12345678", 0.6f, 1001);
        bank.addAccount(cd.getId(), cd);
        boolean actual = withdrawCommandValidator.validate("withdraw 12345678 1001");
        PassTime.passTime(12);
        boolean actual1 = withdrawCommandValidator.validate("withdraw 12345678 1030");
        assertFalse(actual);
        assertTrue(actual1);
    }

    @Test
    void withdraw_from_cd_after_twelve_months_less_than_amount() {
        CD cd = new CD("12345678", 0.6f, 1001);
        bank.addAccount(cd.getId(), cd);
        PassTime.passTime(12);
        boolean actual = withdrawCommandValidator.validate("withdraw 12345678 1001");
        assertFalse(actual);
    }

}
