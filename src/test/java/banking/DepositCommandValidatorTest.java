package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DepositCommandValidatorTest {
    DepositCommandValidator depositCommandValidator;
    Bank bank;

    @BeforeEach
    void setUp() {
        bank = new Bank();
        depositCommandValidator = new DepositCommandValidator(bank);
    }

    @Test
    void invalid_length_deposit_command() {
        Account savings = new Savings("12345678", 0.6f);
        bank.addAccount(savings.getId(), savings);
        boolean actual = depositCommandValidator.validate("deposit 12345678 100 aba");
        boolean actual1 = depositCommandValidator.validate("deposit 12345678");
        boolean actual2 = depositCommandValidator.validate("deposit");
        assertFalse(actual);
        assertFalse(actual1);
        assertFalse(actual2);
    }

    @Test
    void check_deposit_case() {
        Account savings = new Savings("12345678", 0.6f);
        bank.addAccount(savings.getId(), savings);
        boolean actual = depositCommandValidator.validate("Deposit 12345678 500");
        boolean actual1 = depositCommandValidator.validate("DePosit 12345678 500");
        assertTrue(actual);
        assertTrue(actual1);
    }

    @Test
    void check_deposit_ids() {
        Account savings = new Savings("12345678", 0.6f);
        bank.addAccount(savings.getId(), savings);
        boolean actual = depositCommandValidator.validate("Deposit 12345678 500");
        boolean actual1 = depositCommandValidator.validate("Deposit 12345679 500");
        boolean actual2 = depositCommandValidator.validate("Deposit 1234567 500");
        assertTrue(actual);
        assertFalse(actual1);
        assertFalse(actual2);
    }

    @Test
    void check_deposit_id_type() {
        Account savings = new Savings("12345678", 0.6f);
        bank.addAccount(savings.getId(), savings);
        Account checking = new Checking("12345679", 0.6f);
        bank.addAccount(checking.getId(), checking);
        Account cd = new CD("12345689", 0.6f, 1000);
        bank.addAccount(cd.getId(), cd);
        boolean actual = depositCommandValidator.validate("Deposit 12345678 500");
        boolean actual1 = depositCommandValidator.validate("Deposit 12345679 500");
        boolean actual2 = depositCommandValidator.validate("Deposit 12345689 500");
        assertTrue(actual);
        assertTrue(actual1);
        assertFalse(actual2);
    }

    @Test
    void check_deposit_large_sum_savings() {
        Account savings = new Savings("12345678", 0.6f);
        bank.addAccount(savings.getId(), savings);
        boolean actual = depositCommandValidator.validate("Deposit 12345678 2499");
        boolean actual1 = depositCommandValidator.validate("Deposit 12345678 2500");
        boolean actual2 = depositCommandValidator.validate("Deposit 12345678 2501");
        assertTrue(actual);
        assertTrue(actual1);
        assertFalse(actual2);
    }

    @Test
    void check_deposit_large_sum_checking() {
        Account checking = new Checking("12345679", 0.6f);
        bank.addAccount(checking.getId(), checking);
        boolean actual = depositCommandValidator.validate("Deposit 12345679 999");
        boolean actual1 = depositCommandValidator.validate("Deposit 12345679 1000");
        boolean actual2 = depositCommandValidator.validate("Deposit 12345679 1001");
        assertTrue(actual);
        assertTrue(actual1);
        assertFalse(actual2);
    }

    @Test
    void check_deposit_negatives_and_zero() {
        Account savings = new Savings("12345678", 0.6f);
        bank.addAccount(savings.getId(), savings);
        Account checking = new Checking("12345679", 0.6f);
        bank.addAccount(checking.getId(), checking);
        boolean actual = depositCommandValidator.validate("Deposit 12345678 -1");
        boolean actual1 = depositCommandValidator.validate("Deposit 12345678 0");
        boolean actual2 = depositCommandValidator.validate("Deposit 12345679 -1");
        boolean actual3 = depositCommandValidator.validate("Deposit 12345679 0");
        assertFalse(actual);
        assertTrue(actual1);
        assertFalse(actual2);
        assertTrue(actual3);
    }

    @Test
    void check_deposit_null() {
        Account savings = new Savings("12345678", 0.6f);
        bank.addAccount(savings.getId(), savings);
        boolean actual = depositCommandValidator.validate("Deposit 12345678");
        boolean actual1 = depositCommandValidator.validate("Deposit 1234567");
        assertFalse(actual);
        assertFalse(actual1);
    }

    @Test
    void check_deposit_string() {
        Account savings = new Savings("12345678", 0.6f);
        bank.addAccount(savings.getId(), savings);
        Account checking = new Checking("12345679", 0.6f);
        bank.addAccount(checking.getId(), checking);
        boolean actual = depositCommandValidator.validate("Deposit 12345678 yes");
        boolean actual1 = depositCommandValidator.validate("Deposit 12345679 yes");
        assertFalse(actual);
        assertFalse(actual1);
    }

    @Test
    void try_deposit_into_cd_does_not_work() {
        Account cd = new CD("12345678", 0.6f, 1000);
        bank.addAccount(cd.getId(), cd);
        boolean actual = depositCommandValidator.validate("Deposit 12345678 100");
        assertFalse(actual);
    }
}
