package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DepositCommandProcessorTest {
    public static final String[] INPUT1 = new String[]{"create", "checking", "12345678", "1.0"};
    public static final String[] INPUT2 = new String[]{"create", "savings", "12345679", "1.0"};
    public static final String[] INPUT3 = new String[]{"create", "cd", "12345688", "1.0", "1000"};
    public static final String[] DINPUT1 = new String[]{"deposit", "12345678", "678"};
    public static final String[] DINPUT2 = new String[]{"deposit", "12345679", "678"};
    public static final String[] DINPUT3 = new String[]{"deposit", "12345678", "99"};
    public static final String[] DINPUT4 = new String[]{"deposit", "12345679", "99"};

    Bank bank;
    CreateCommandProcessor createCommandProcessor;
    DepositCommandProcessor depositCommandProcessor;

    @BeforeEach
    void setUp() {
        bank = new Bank();
        createCommandProcessor = new CreateCommandProcessor(bank);
        depositCommandProcessor = new DepositCommandProcessor(bank);
        createCommandProcessor.processInput(INPUT1);
        createCommandProcessor.processInput(INPUT2);
        createCommandProcessor.processInput(INPUT3);
    }

    @Test
    void check_deposit_checking() {
        depositCommandProcessor.processInput(DINPUT1);
        assertEquals(new BigDecimal("678.00"), bank.getAccounts().get("12345678").getBalance());
    }

    @Test
    void check_deposit_savings() {
        depositCommandProcessor.processInput(DINPUT2);
        assertEquals(new BigDecimal("678.00"), bank.getAccounts().get("12345679").getBalance());
    }

    @Test
    void check_multi_deposit_checking() {
        depositCommandProcessor.processInput(DINPUT1);
        depositCommandProcessor.processInput(DINPUT1);
        assertEquals(new BigDecimal("1356.00"), bank.getAccounts().get("12345678").getBalance());
    }

    @Test
    void check_multi_deposit_savings() {
        depositCommandProcessor.processInput(DINPUT2);
        depositCommandProcessor.processInput(DINPUT2);
        assertEquals(new BigDecimal("1356.00"), bank.getAccounts().get("12345679").getBalance());
    }
}
