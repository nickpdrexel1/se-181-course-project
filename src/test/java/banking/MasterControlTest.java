package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MasterControlTest {

    MasterControl masterControl;
    List<String> input;

    @BeforeEach
    void setUp() {
        input = new ArrayList<>();
        Bank bank = new Bank();
        masterControl = new MasterControl(bank, new CommandValidator(bank), new CommandProcessor(bank), new CommandStorage(), new ValidCommandStorage(bank));
    }

    private void assertOnSingleCommand(String command, List<String> actual) {
        assertEquals(1, actual.size());
        assertEquals(command, actual.get(0));
    }

    @Test
    void create_spelled_wrong_in_command_is_invalid() {
        input.add("creat checking 12345678 1.0");

        List<String> actual = masterControl.start(input);

        assertOnSingleCommand("creat checking 12345678 1.0", actual);
    }

    @Test
    void deposit_spelled_wrong_in_command_is_invalid() {
        input.add("deopsit 12345678 565");

        List<String> actual = masterControl.start(input);

        assertOnSingleCommand("deopsit 12345678 565", actual);
    }

    @Test
    void pass_spelled_wrong_in_command_is_invalid() {
        input.add("pyass 1");

        List<String> actual = masterControl.start(input);

        assertOnSingleCommand("pyass 1", actual);
    }

    @Test
    void withdraw_spelled_wrong_in_command_is_invalid() {
        input.add("wyithdraw 12345678 100");

        List<String> actual = masterControl.start(input);

        assertOnSingleCommand("wyithdraw 12345678 100", actual);
    }

    @Test
    void two_commands_typos_invalid() {
        input.add("creat checking 12345678 1.0");
        input.add("deopsit 12345678 565");

        List<String> actual = masterControl.start(input);

        assertEquals(2, actual.size());
        assertEquals("creat checking 12345678 1.0", actual.get(0));
        assertEquals("deopsit 12345678 565", actual.get(1));
    }

    @Test
    void cannot_create_accounts_with_same_ID() {
        input.add("create checking 12345678 1.0");
        input.add("create checking 12345678 1.0");

        List<String> actual = masterControl.start(input);
        assertEquals("create checking 12345678 1.0", actual.get(1));
    }

    @Test
    void cannot_withdraw_from_savings_twice_in_same_month() {
        input.add("create savings 12345678 1.0");
        input.add("deposit 12345678 200");
        input.add("withdraw 12345678 100");
        input.add("withdraw 12345678 100");

        List<String> actual = masterControl.start(input);
        assertEquals("Savings 12345678 100.00 1.00", actual.get(0));
        assertEquals("deposit 12345678 200", actual.get(1));
        assertEquals("withdraw 12345678 100", actual.get(2));
        assertEquals("withdraw 12345678 100", actual.get(3));
    }

    @Test
    void pass_max_time_on_all_accounts_try_pass_greater() {
        input.add("create savings 12345678 1.0");
        input.add("create checking 12345679 1.0");
        input.add("create cd 12345680 1.0 1000");
        input.add("deposit 12345678 200");
        input.add("deposit 12345679 200");
        input.add("pass 60");
        input.add("pass 61");

        List<String> actual = masterControl.start(input);
        assertEquals("Savings 12345678 210.24 1.00", actual.get(0));
        assertEquals("deposit 12345678 200", actual.get(1));
        assertEquals("Checking 12345679 210.24 1.00", actual.get(2));
        assertEquals("deposit 12345679 200", actual.get(3));
        assertEquals("Cd 12345680 1221.30 1.00", actual.get(4));
        assertEquals("pass 61", actual.get(5));
    }

    @Test
    void withdraw_from_two_separate_savings_accounts_in_same_month() {
        input.add("create savings 12345678 1.0");
        input.add("create savings 12345679 1.0");
        input.add("deposit 12345678 200");
        input.add("deposit 12345679 200");
        input.add("withdraw 12345678 100");
        input.add("withdraw 12345679 100");
        input.add("withdraw 12345678 100");
        input.add("withdraw 12345679 100");

        List<String> actual = masterControl.start(input);
        assertEquals("Savings 12345678 100.00 1.00", actual.get(0));
        assertEquals("deposit 12345678 200", actual.get(1));
        assertEquals("withdraw 12345678 100", actual.get(2));
        assertEquals("Savings 12345679 100.00 1.00", actual.get(3));
        assertEquals("deposit 12345679 200", actual.get(4));
        assertEquals("withdraw 12345679 100", actual.get(5));
        assertEquals("withdraw 12345678 100", actual.get(6));
        assertEquals("withdraw 12345679 100", actual.get(7));
    }

    @Test
    void does_transfer_stay_in_account_after_one_is_deleted() {
        input.add("create savings 12345678 1.0");
        input.add("create savings 12345679 1.0");
        input.add("deposit 12345678 200");
        input.add("deposit 12345679 100");
        input.add("transfer 12345679 12345678 100");
        input.add("pass 1");
        List<String> actual = masterControl.start(input);
        assertEquals("Savings 12345678 300.24 1.00", actual.get(0));
        assertEquals("deposit 12345678 200", actual.get(1));
        assertEquals("transfer 12345679 12345678 100", actual.get(2));
    }

    @Test
    void test_deposit_and_withdraw_on_cd_before_and_after_twelve_months() {
        input.add("create cd 12345680 1.0 1000");
        input.add("deposit 12345680 200");
        input.add("withdraw 12345680 201");
        input.add("pass 6");
        input.add("deposit 12345680 202");
        input.add("withdraw 12345680 203");
        input.add("pass 6");
        input.add("deposit 12345680 204");
        input.add("withdraw 12345680 205");
        input.add("withdraw 12345680 2000");

        List<String> actual = masterControl.start(input);
        assertEquals("Cd 12345680 0.00 1.00", actual.get(0));
        assertEquals("withdraw 12345680 2000", actual.get(1));
        assertEquals("deposit 12345680 200", actual.get(2));
        assertEquals("withdraw 12345680 201", actual.get(3));
        assertEquals("deposit 12345680 202", actual.get(4));
        assertEquals("withdraw 12345680 203", actual.get(5));
        assertEquals("deposit 12345680 204", actual.get(6));
        assertEquals("withdraw 12345680 205", actual.get(7));
    }

    @Test
    void sample_make_sure_this_passes_unchanged_or_you_will_fail() {
        input.add("Create savings 12345678 0.6");
        input.add("Deposit 12345678 700");
        input.add("Deposit 12345678 5000");
        input.add("creAte cHecKing 98765432 0.01");
        input.add("Deposit 98765432 300");
        input.add("Transfer 98765432 12345678 300");
        input.add("Pass 1");
        input.add("Create cd 23456789 1.2 2000");
        List<String> actual = masterControl.start(input);

        assertEquals(5, actual.size());
        assertEquals("Savings 12345678 1000.50 0.60", actual.get(0));
        assertEquals("Deposit 12345678 700", actual.get(1));
        assertEquals("Transfer 98765432 12345678 300", actual.get(2));
        assertEquals("Cd 23456789 2000.00 1.20", actual.get(3));
        assertEquals("Deposit 12345678 5000", actual.get(4));
    }

}
