package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PassTimeCommandValidatorTest {

    PassTimeCommandValidator passTimeCommandValidator;
    Bank bank;

    @BeforeEach
    void setUp() {
        bank = new Bank();
        passTimeCommandValidator = new PassTimeCommandValidator(bank);
    }

    @Test
    void invalid_length_pass_time_command() {
        Account savings = new Savings("12345678", 0.6f);
        bank.addAccount(savings.getId(), savings);
        boolean actual = passTimeCommandValidator.validate("pass 1 aba");
        boolean actual1 = passTimeCommandValidator.validate("pass");
        assertFalse(actual);
        assertFalse(actual1);
    }

    @Test
    void check_pass_time_input() {
        boolean actual = passTimeCommandValidator.validate("Pass 1");
        boolean actual2 = passTimeCommandValidator.validate("pass oogabooga");
        assertTrue(actual);
        assertFalse(actual2);
    }

    @Test
    void pass_time_input_bounds() {
        boolean actual = passTimeCommandValidator.validate("Pass 0");
        boolean actual1 = passTimeCommandValidator.validate("Pass 1");
        boolean actual2 = passTimeCommandValidator.validate("Pass 2");
        boolean actual3 = passTimeCommandValidator.validate("Pass 60");
        boolean actual4 = passTimeCommandValidator.validate("Pass 61");
        assertFalse(actual);
        assertTrue(actual1);
        assertTrue(actual2);
        assertTrue(actual3);
        assertFalse(actual4);
    }
}
